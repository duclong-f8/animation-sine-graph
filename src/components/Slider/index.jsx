import classNames from "classnames/bind";
import { useEffect, useMemo, useRef, useState } from "react";
import styles from "./Slider.module.scss";

const cx = classNames.bind(styles);

function Slider() {
  const items = useRef([-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6]);
  const [translateX, setTranslateX] = useState(0);

  const prevScrollY = useRef(0);

  const displayItems = useMemo(() => {
    return items.current.map((item) => {
      const x = item * 200 + translateX;
      return {
        x,
        y: 90 * Math.sin(x / 90),
      };
    });
  }, [translateX]);

  useEffect(() => {
    const handle = () => {
      const scrollY = window.scrollY;

      setTranslateX(translateX + (scrollY - prevScrollY.current));

      prevScrollY.current = scrollY;
    };

    window.addEventListener("scroll", handle);

    return () => window.removeEventListener("scroll", handle);
  }, [translateX]);
  return (
    <div className={cx("wrapper")}>
      <div className={cx("root")} style={{ "--translateX": `${translateX}px` }}>
        {displayItems.map((item, index) => (
          <div
            key={index}
            className={cx("box")}
            style={{ "--x": `${item.x}px`, "--y": `${item.y}px` }}
          >
            box {index + 1}
          </div>
        ))}
      </div>
    </div>
  );
}

export default Slider;
